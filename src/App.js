import { useState } from "react";
import "./App.css";
import InputData from "./Components/InputData/InputData";
import Posts from "./Components/CardListRender/Posts";
import Pagination from "./Components/Pagination/Pagination";

function App() {
  const [formInputData, setFormInputData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const cardsPerPage = 2;
  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentPosts = formInputData.slice(indexOfFirstCard, indexOfLastCard);

  const submitData = (enteredData) => {
    setFormInputData([enteredData, ...formInputData]);
  };

  const removeHandler = (id) => {
    const newFormData = formInputData.filter((input) => input.id !== id);
    setFormInputData(newFormData);
  };

  const updateHandler = (updatedDetails) => {
    const updatedFormData = [...formInputData];
    for (let i = 0; i < updatedFormData.length; i++) {
      if (updatedFormData[i].id === updatedDetails.id) {
        updatedFormData[i].title = updatedDetails.title;
        updatedFormData[i].content = updatedDetails.content;
      }
    }
    setFormInputData(updatedFormData);
  };

  return (
    <div className="App">
      <InputData onSubmit={submitData} />
      <Posts
        formData={currentPosts}
        onRemove={removeHandler}
        onUpdate={updateHandler}
      />
      <Pagination
        cardsPerPage={cardsPerPage}
        totalCards={formInputData.length}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </div>
  );
}

export default App;
