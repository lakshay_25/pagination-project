import React, { useState } from "react";
import UserForm from "../UI/UserForm";

let idCounter = 1;

function InputData(props) {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const cancelHandler = () => {
    setTitle("");
    setContent("");
  };

  const fieldDetails = {
    title: {
      id: "title",
      label: "Enter Title",
      type: "text",
      value: title,
      onChange(e) {
        setTitle(e.target.value);
      },
    },
    contentArea: {
      id: "content",
      label: "Enter Content",
      rows: "10",
      value: content,
      onChange(e) {
        setContent(e.target.value);
      },
    },
    submitButton: {
      name: "Submit",
    },
    cancelButton: {
      name: "Cancel",
    },
  };

  const submitHandler = (e) => {
    e.preventDefault();
    
    if (title.trim() === "" || content.trim() === "") {
      alert("Please enter Valid Details!!");
      return;
    }
    props.onSubmit({
      id: idCounter++,
      title,
      content,
    });
    setTitle("");
    setContent("");
  };

  return (
    <UserForm
      fieldDetails={fieldDetails}
      onSubmit={submitHandler}
      onClick={cancelHandler}
    />
  );
}

export default InputData;
