import styles from "./UserForm.module.css";
import Button from "./Button";

function UserForm(props) {
  const classes = `${styles["form-control"]} ${props.className}`;

  const { fieldDetails, onSubmit, onClick } = props;

  const titleChangeHandler = (e) => {
    fieldDetails.title.onChange(e);
  };

  const contentChangeHandler = (e) => {
    fieldDetails.contentArea.onChange(e);
  };

  return (
    <form className={classes} onSubmit={onSubmit}>
      <div>
        <label htmlFor={fieldDetails.title.id}>
          {fieldDetails.title.label}
        </label>
        <input
          type={fieldDetails.title.type}
          id={fieldDetails.title.id}
          onChange={titleChangeHandler}
          value={fieldDetails.title.value}
        />
      </div>
      <div>
        <label htmlFor={fieldDetails.contentArea.id}>
          {fieldDetails.contentArea.label}
        </label>
        <textarea
          id={fieldDetails.contentArea.id}
          rows={fieldDetails.contentArea.rows}
          value={fieldDetails.contentArea.value}
          onChange={contentChangeHandler}
        ></textarea>
      </div>
      <div className={classes["btn-div"]}>
        <Button>{fieldDetails.submitButton.name}</Button>
        <Button type="button" onClick={onClick}>
          {fieldDetails.cancelButton.name}
        </Button>
      </div>
    </form>
  );
}

export default UserForm;
