import styles from "./Button.module.css";

function Button(props) {
  const classes = `${styles.button} ${props.className}`;

  return (
    <button className={classes} onClick={props.onClick} type={props.type}>
      {props.children}
    </button>
  );
}

export default Button;
