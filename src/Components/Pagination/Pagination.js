import classes from "./Pagination.module.css";

function Pagination(props) {
  const { cardsPerPage, totalCards, setCurrentPage, currentPage } = props;
  let disablePreviousButton = false;
  let disableNextButton = false;
  const totalPages = totalCards ? Math.ceil(totalCards / cardsPerPage) : null;

  if (currentPage === 1) {
    disablePreviousButton = true;
  }

  if (currentPage === totalPages) {
    disableNextButton = true;
  }

  const previousPageHandler = () => {
    setCurrentPage(currentPage - 1);
  };

  const nextPageHandler = () => {
    setCurrentPage(currentPage + 1);
  };

  if (!totalPages) {
    return <> </>;
  }

  return (
    <div className={classes["pagination"]}>
      <button onClick={previousPageHandler} disabled={disablePreviousButton}>
        Previous
      </button>
      <div>
        <span>{currentPage}</span>
        <span>of</span>
        <span>{totalPages}</span>
      </div>
      <button onClick={nextPageHandler} disabled={disableNextButton}>
        Next
      </button>
    </div>
  );
}

export default Pagination;
