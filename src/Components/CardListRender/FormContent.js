import { useState, Fragment } from "react";
import Card from "../UI/Card";
import Button from "../UI/Button";
import classes from "./FormContent.module.css";
import UserForm from "../UI/UserForm";

function FormContent(props) {
  const [title, setTitle] = useState(props.title);
  const [content, setContent] = useState(props.content);
  const [isEdit, setIsEdit] = useState(false);

  const updateHandler = () => {
    setIsEdit(true);
  };

  const removeHandler = () => {
    props.onRemove(props.id);
  };

  const updateCancelHandler = () => {
    setTitle(props.title);
    setContent(props.content);
    setIsEdit(false);
  };

  const updateDetailsHandler = (e) => {
    e.preventDefault();
    if(title.trim() === "" || content.trim() === ""){
      alert("Please Enter Valid Details!");
      return;
    }
    props.onUpdate({
      id: props.id,
      title: title,
      content: content,
    });
    setIsEdit(false);
  };

  const fieldDetails = {
    title: {
      id: "title",
      label: "Enter New Title",
      type: "text",
      value: title,
      onChange(e) {
        setTitle(e.target.value);
      },
    },
    contentArea: {
      id: "content",
      label: "Enter New Content",
      rows: "10",
      value: content,
      onChange(e) {
        setContent(e.target.value);
      },
    },
    submitButton: {
      name: "Save",
    },
    cancelButton: {
      name: "Cancel",
    },
  };

  return (
    <Fragment>
      {isEdit ? (
        <UserForm
          fieldDetails={fieldDetails}
          onSubmit={updateDetailsHandler}
          onClick={updateCancelHandler}
        />
      ) : (
        <Card>
          <div className={classes.content}>
            <h3>{props.title}</h3>
            <h5>{props.content}</h5>
          </div>
          <div className={classes["btn-div"]}>
            <Button onClick={updateHandler} className={classes.button}>
              Update
            </Button>
            <Button onClick={removeHandler} className={classes.button}>
              Remove
            </Button>
          </div>
        </Card>
      )}
    </Fragment>
  );
}

export default FormContent;
