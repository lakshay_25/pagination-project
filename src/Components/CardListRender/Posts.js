import { Fragment } from "react";
import FormContent from "./FormContent";
import classes from './RenderFormContent.module.css';

function Posts(props) {
  const inputData = props.formData.map((data) => (
    <FormContent
      key={data.id}
      id={data.id}
      title={data.title}
      content={data.content}
      onRemove={props.onRemove}
      onUpdate={props.onUpdate}
    />
  ));

  return (
    <Fragment>
      {inputData.length !== 0 ? inputData : <p className = {classes.p}>No Items Added.</p>}
    </Fragment>
  );
}

export default Posts;
